FROM alpine:3.10

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
RUN addgroup -S mysql && adduser -S -G mysql mysql

# add gosu for easy step-down from root
RUN apk add --no-cache gosu --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing \
  # verify that the binary works
  && gosu nobody true

# install "bash" for docker-entrypoint.sh
# install "gzip" for docker-entrypoint-initdb.d
# install "pwgen" for randomizing passwords
# install "tzdata" for /usr/share/zoneinfo
ARG MARIADB_VERSION=10.3.17-r0
RUN set -ex; \
  apk add --no-cache \
    bash \
    gzip \
    "mariadb>${MARIADB_VERSION}" \
    "mariadb-client>${MARIADB_VERSION}" \
    pwgen \
    tzdata; \
  rm -rf /var/cache/apk/*; \
# comment out any "user" entires in the MySQL config ("docker-entrypoint.sh" or "--user" will handle user switching)
  sed -ri 's/^user\s/#&/' /etc/my.cnf /etc/my.cnf.d/*; \
# purge and re-create /var/lib/mysql with appropriate ownership
  rm -rf /var/lib/mysql /run/mysqld; \
  mkdir -p /var/lib/mysql /run/mysqld; \
  chown -R mysql:mysql /var/lib/mysql /run/mysqld; \
# ensure that /var/run/mysqld (used for socket and lock files) is writable regardless of the UID our mysqld instance ends up having at runtime
  chmod 777 /run/mysqld; \
# comment out a few problematic configuration values
  # find /etc/my.cnf.d/ -name '*.cnf' -print0 \
  # 	| xargs -0 grep -lZE '^(bind-address|log)' \
  # 	| xargs -rt -0 sed -Ei 's/^(bind-address|log)/#&/'; \
# don't reverse lookup hostnames, they are usually another container
  echo -e '[mysqld]\nskip-host-cache\nskip-name-resolve' > /etc/my.cnf.d/docker.cnf; \
# make port and socket available to client
  echo -e '[client]\nport = 3306\nsocket = /run/mysqld/mysqld.sock' > /etc/my.cnf

VOLUME /var/lib/mysql

RUN mkdir /docker-entrypoint-initdb.d

COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
EXPOSE 3306

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["mysqld"]
