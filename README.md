# daverona/mariadb

[![pipeline status](https://gitlab.com/toscana/docker/mariadb/badges/master/pipeline.svg)](https://gitlab.com/toscana/docker/mariadb/commits/master)

* GitLab source repository: [https://gitlab.com/toscana/docker/mariadb](https://gitlab.com/toscana/docker/mariadb)
* Docker Hub repository: [https://hub.docker.com/r/daverona/mariadb](https://hub.docker.com/r/daverona/mariadb)

This is a Docker image of MariaDB on Alpine Linux. This image provides:

* [MariaDB](https://mariadb.org/) 10.3.17
* [Alpine Linux](https://alpinelinux.org) 3.10

This image has been built in such a way that it is *almost identical* in logic 
to the official docker image of MariaDB 10.4 on Ubuntu 18.04 
([mariadb:10.3.17-bionic](https://github.com/docker-library/mariadb/blob/58a336f5cfeb4e33ae28e59178cb25e2ce7991e6/10.3/Dockerfile))
but it is *about half* in size (187 MB) of the office image (343 MB).
Thus you can use all the features the official image provides. 

But nothing comes free. If you are going to mount a native directory instead of
a Docker volume to /var/lib/mysql, where MariaDB stores all its files, 
on macOS or Windows, this image won't work.


## Installation

Install [Docker](https://hub.docker.com/search/?type=edition&offering=community)
if you don't have one. Then pull the image from Docker Hub repository:

```bash
docker image pull daverona/mariadb
```

or build the image:

```bash
docker image build \
  --tag daverona/mariadb \
  .
```

## Usage

Please refer to 
[https://hub.docker.com/_/mariadb?tab=description](https://hub.docker.com/_/mariadb?tab=description).

## References

* [https://mariadb.org/](https://mariadb.org/)
* [https://hub.docker.com/_/mariadb](https://hub.docker.com/_/mariadb)
* [https://github.com/docker-library/mariadb/tree/master/10.3](https://github.com/docker-library/mariadb/tree/master/10.3)
